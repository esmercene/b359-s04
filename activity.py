from abc import ABC, abstractmethod


class Animal(ABC):
    
    @abstractmethod
    def eat(self, food):
        pass
    
    @abstractmethod
    def make_sound(self):
        pass


#  Dog and Cat classes 
class Dog(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age
    
    def eat(self, food):
        print(f"{self._name} is eating {food}")
    
    def make_sound(self):
        print(f"{self._name} says Woof!, Arf!, Awoooooo000!")
    
    def call(self):
        print(f"Come here, {self._name}!")
    
    # Getter Methods 
    def get_name(self):
        print(self._name)
    
    def get_breed(self):
        print(self._breed)
    
    def get_age(self):
        print(self._age)

    def get_details(self):
        print(f"{self._name} is a {self._age} year old {self._breed} dog.")


class Cat(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age
    
    def eat(self, food):
        print(f"{self._name} is eating {food}")
    
    def make_sound(self):
        print(f"{self._name} says Meow, Meow, Nyaaaaaaaaww!")
    
    def call(self):
        print(f"Come here, {self._name}!")
    
    # Getter Methods 
    def get_name(self):
        print(self._name)
    
    def get_breed(self):
        print(self._breed)
    
    def get_age(self):
        print(self._age)

    def get_details(self):
        print(f"{self._name} is a {self._age} year old {self._breed} cat.")


dog1 = Dog("Isis", "Dalmatian", 15)
dog1.get_details()
dog1.eat("Steak")
dog1.make_sound()
dog1.call()


cat1 = Cat("Puss", "Persian", 4)
cat1.get_details()
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()

